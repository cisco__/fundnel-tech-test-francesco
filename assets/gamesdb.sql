-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 25, 2015 at 01:04 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gamesdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `createDate` datetime NOT NULL,
  `creatorId` int(11) NOT NULL COMMENT 'This related with users',
  `gameCover` varchar(100) NOT NULL,
  `descr` text NOT NULL,
  `developer` varchar(50) NOT NULL,
  `publisher` varchar(50) NOT NULL,
  `genreID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creatorId` (`creatorId`),
  KEY `genreID` (`genreID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `name`, `createDate`, `creatorId`, `gameCover`, `descr`, `developer`, `publisher`, `genreID`) VALUES
(2, 'Fast N Furiosu', '2015-12-21 00:00:00', 2, 'xxxxx.jpg', 'This is bla bla bla', 'Gameloft', 'Gramedia', 2),
(3, 'Game ke dua', '2015-12-21 00:00:00', 3, 'hua.jpg', 'ini adalah ke dua game', 'gamejuga', 'safari', 2),
(4, 'in untuk kebutuhan testing', '2015-12-23 00:00:00', 1, 'akakom.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(5, 'in untuk kebutuhan testing 2', '2015-12-25 11:36:32', 1, 'laper.jpg', 'INi adlah kebutuhan description 2', 'Mangga Besar 2', 'Mangga Dua 2', 2),
(6, 'testing', '2015-12-25 12:02:14', 1, 'aqua_tanggung.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(7, 'testing', '2015-12-23 00:00:00', 1, '544242_10151511245934643_1454672322_n.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(8, 'testing', '2015-12-23 00:00:00', 1, '544242_10151511245934643_1454672322_n.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(9, 'testing', '2015-12-23 00:00:00', 1, '544242_10151511245934643_1454672322_n.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(10, 'testing', '2015-12-23 00:00:00', 1, '544242_10151511245934643_1454672322_n.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(11, 'testing', '2015-12-23 00:00:00', 1, 'aqua_tanggung.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(12, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(13, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(14, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(15, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(16, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(17, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(18, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(19, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(20, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(21, 'testing', '2015-12-23 00:00:00', 1, 'awesome.jpg', 'INi adlah kebutuhan description', 'Mangga Besar', 'Mangga Dua', 2),
(22, 'Maria Ivanka', '2015-12-24 19:57:34', 1, 'Celine-Dion-Its-All-Coming-Back-To-Me-Now-Single-Cover.jpg', 'Anak Kami', 'Cisco and Vinna', 'God', 2),
(23, 'Maria Ivanka 2', '2015-12-24 19:58:08', 1, 'dcc-tshirt-2.png', 'Anak Kami', 'Cisco and Vinna', 'God', 1),
(24, 'ini terakhir', '2015-12-25 11:26:19', 1, 'Hammer Guy. wallpaper.jpg', 'coba aja', 'coba dev', 'coba pub', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gameComments`
--

CREATE TABLE IF NOT EXISTS `gameComments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gameID` int(11) NOT NULL,
  `comments` text NOT NULL,
  `userID` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gameID` (`gameID`,`userID`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `gameComments`
--

INSERT INTO `gameComments` (`id`, `gameID`, `comments`, `userID`, `createDate`) VALUES
(1, 2, 'In front of a live global audience this morning (Dec 21, Singapore time) the wrong winner for Miss Universe was announced.\r\n\r\nThe host of of the pageant, comedian Steve Harvey initially announced that Miss Colombia, Ariadna Gutierrez won the pageant.\r\n\r\nAfter he realised his error, he apologised and then quickly announced that the actual winner was Miss Philippines Pia Alonzo Wurtzbach.\r\n\r\n- See more at: http://www.tnp.sg/news/world-news/after-mix-miss-philippines-crowned-miss-universe-2015#sthash.2WUIpXV4.dpuf', 4, '2015-12-21 00:00:00'),
(2, 2, 'He could be the right man or the man for right now, which leaves Manchester United staring at Sophie''s Choice.\r\n\r\nAs Louis van Gaal braces himself for a defining 24 hours over his tenure at one of the biggest sporting institutions in the world, the identity of his successor poses an increasing conundrum.\r\n\r\nFootball''s managerial merry-go-round has placed the game''s two most coveted men in contention to replace the beleaguered Dutchman.\r\n\r\nIt is a predicament which the Old Trafford hierarchy can ill afford to get wrong.\r\n\r\nIn an era already blighted by successive misguided appointments since Sir Alex Ferguson''s retirement, a third error of judgment could have far-reaching implications on the club''s stature.\r\n\r\nVan Gaal may have already feared the worst as he trudged off after the final whistle, with the boos from the stands ringing in his ears following the embarrassing 2-1 loss to Norwich City at Old Trafford on Saturday.\r\n\r\nUnited''s desire to become winners again still seems a long way off as they have now gone six games without a win and many blame it on the pragmatism and predictability of van Gaal''s methodology.\r\n\r\nIGNORED\r\n\r\nFollowing 18 months where the only success has been derived from possession statistics, there is no longer a willingness among many of the club''s faithful to "pay attention to the manager".\r\n\r\nThat spells danger for the Dutchman and if the board decide his time is up, then Jose Mourinho represents a logical fit for United in both timing and persona.\r\n\r\nChelsea''s newly-deposed manager has sent out a clear message that he still has business to tend to in the English Premier League.\r\n\r\nShould he take the reins, the flair synonymous with the much-vaunted "United way" will not make a return.\r\n\r\nOther institutional hallmarks would invariably be sacrificed for his blueprint, notably the club''s long-standing policy of promoting home-grown prospects.\r\n\r\nBut the Portuguese will instil traits last seen in the days of Ferguson''s all-conquering sides by again overpowering opponents through a combination of strength and tactical guile, both currently conspicuous by their absence under van Gaal.\r\n\r\nCRAFTED\r\n\r\nMourinho''s path to Old Trafford has been carefully crafted ever since his celebratory touchline sprint as Porto knocked United out of the Champions League in 2004.\r\n\r\nHe has generally avoided the antagonism and faux pas that culminated in his current level of infamy with clubs like Arsenal, Liverpool and even Barcelona.\r\n\r\nHis rapid fall from grace and alienation in the dressing room in his final months at Stamford Bridge provides an obvious cause for concern, particularly given the acrimonious circumstances in which Juan Mata was allowed to depart for United.\r\n\r\nSo, too, does his short-term approach which inevitably unravels by the third full season, as it did again last week.\r\n\r\nBut the parameters at United differ greatly from those at Chelsea, Real Madrid and Inter Milan.\r\n\r\nPower reverts to one source - the manager.\r\n\r\nDespite the club''s grandiose reputation, with three-tiered stands and regular sightings of the rich and famous sitting in the plush and expensive seats at Old Trafford, and money-spinning partnerships across the globe, United remain enslaved to a traditional footballing ethos.\r\n\r\nIt is why there is an apprehension to be drawn into the unsightly business of technical directors and a rapid fire-and-hire policy, which has seen United stop short of jettisoning van Gaal.\r\n\r\nThey have never been afraid to sack managers mid-season, just ask Wilf McGuinness, Frank O''Farrell and Ron Atkinson. However, pulling the trigger on two in three seasons would erode at the mystique behind the power of the throne.\r\n\r\nAs the football world continues to hold its breath over Pep Guardiola''s next port of call, United cannot afford to stand still.\r\n\r\nBayern Munich''s outgoing manager is unlikely to be enticed by the challenge of major rebuilding work in the red half of Manchester.\r\n\r\nThe prospect of a far comfortable existence, aided by the familiarity of his former Barcelona aides in the blue half, makes United and Mourinho increasingly compatible bedfellows.\r\n\r\nHe is a serial winner and right now, he may actually be as good as it gets.\r\n\r\n- See more at: http://www.tnp.sg/sports/football/jose-perfect-united-if-van-gaal-leaves#sthash.SOo0JrE5.dpuf', 3, '2015-12-21 00:00:00'),
(3, 4, 'ksksk sksksks', 1, '0000-00-00 00:00:00'),
(4, 5, 'hollywood', 1, '2015-12-24 21:51:31'),
(5, 5, 'hollywood 2', 1, '2015-12-24 21:57:09'),
(6, 2, 'asdsad', 1, '2015-12-25 12:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `gameGenre`
--

CREATE TABLE IF NOT EXISTS `gameGenre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gameGenre`
--

INSERT INTO `gameGenre` (`id`, `name`) VALUES
(1, 'Shooter'),
(2, 'Racing'),
(3, 'Adventure');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'Francesco Xavier Kolly Mally', 'francescokm@gmail.com', 'secretpassword'),
(2, 'Ivana Dione', 'ivanadione@gmail.com', 'hore'),
(3, 'Alex Luttu', 'allex.luttu@gmail.com', 'emanghebatyah'),
(4, 'asasasasas asdasdasdsa', 'francescokm@gmail.com', 'secretpassword');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `game_ibfk_2` FOREIGN KEY (`genreID`) REFERENCES `gameGenre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gameComments`
--
ALTER TABLE `gameComments`
  ADD CONSTRAINT `gameComments_ibfk_1` FOREIGN KEY (`gameID`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gameComments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
