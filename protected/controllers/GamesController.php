<?php
  
  date_default_timezone_set('Asia/Jakarta');

class GamesController extends Controller
{
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	
	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Games');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionView($id)
	{
		$people = GameComments::model()->findAllByAttributes(array('gameID'=>$id));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'modelDua'=>$people,
		));
	}

	public function loadModel($id)
	{
		$model=Games::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	

	public function actionGetFromAjax()
	{
		$model= new GameComments;
		$model->gameID=Yii::app()->request->getPost('game');
		$model->comments=Yii::app()->request->getPost('comments');
		$model->userID=Yii::app()->request->getPost('user');
		$model->createDate=date('Y-m-d:H:i:s');
		$model->save(false);
		echo json_encode("OK");
	}

	public function actionCreate()
	{
		if(Yii::app()->session['idUser'])
		{
			$model=new Games;
			$modelGenre = new GameGenre;
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			$allGenre = CHtml::listData(GameGenre::model()->findAll(),'id', 'name');
			
			if(isset($_POST['Games']))
			{
				$file = CUploadedFile::getInstance($model,'gameCover');
				$model->attributes=$_POST['Games'];
				$model->gameCover=$file->getName();
				$model->genreID=Yii::app()->request->getPost('GameGenres');
				if($model->save())
				{
					if(is_object($file))
					{
						$objek = Yii::app()->basePath;
						$objek = str_replace("protected","",$objek);
						$objek = $objek.'images/'.$file->getName();
		         		$file->saveAs($objek);
		         		$this->redirect(array('view','id'=>$model->id));
					}
				}
			}
			
			$this->render('create',array(
				'model'=>$model,
				'modelGenre' => CHtml::dropDownList('GameGenres','GameGenre',$allGenre,array('empty' => 'Select a Genre'))
			));
		}
	}

	public function actionUpdate($id)
	{
		
		$file="";
		$objek="";
		$model=$this->loadModel($id);
		if($model->creatorId == Yii::app()->session['idUser'])
		{
			$allGenre = CHtml::listData(GameGenre::model()->findAll(),'id', 'name');
			$oldCover = $model->gameCover;
			if(isset($_POST['Games']))
			{
				$file = CUploadedFile::getInstance($model,'gameCover');
				$model->attributes=$_POST['Games'];
		
				if(isset($file))
				{
					
					$model->gameCover = $file->getName();
				}elseif($file ==null)
				{

					$model->gameCover = $oldCover;
					var_dump($oldCover);
				}

				$model->creatorId=Yii::app()->session['idUser'];
				$model->createDate=date('Y-m-d:H:i:s');
				$model->genreID=Yii::app()->request->getPost('GameGenres');
				
				if($model->save())
				{
					if(is_object($file))
					{
						$objek = Yii::app()->basePath;
						$objek = str_replace("protected","",$objek);
						$objek = $objek.'images/'.$file->getName();
			         	$file->saveAs($objek);	
					}
					$this->redirect(array('view','id'=>$model->id));
				}
				
			}
				$this->render('update',array(
						'model'=>$model,
						'modelGenre' => CHtml::dropDownList('GameGenres','GameGenre',$allGenre,array('options' => array($model->genreID=>array('selected'=>true))))
					));
		}
	}
}