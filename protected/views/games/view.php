<?php
/* @var $this GamesController */
/* @var $model Games */

$this->breadcrumbs=array(
	'Games'=>array('index'),
	$model->name,
);
?>

<h1>View Games ID #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'createDate',
		array(
				'label' => 'Creator',
				'value' => CHtml::encode($model->creator->name)
			),
		'gameCover',
		'descr',
		'developer',
		'publisher',
		array(
				'label' => 'Genre',
				'value' => CHtml::encode($model->genre->name)
			),
/*
		array(
				'label' => 'Comments',
				'value' => CHtml::encode($komen)
			), 
			*/

	),
));
echo "<h1>Comments</h1>";
echo "<ul>";
$komen = "";
foreach ($modelDua as $key => $value) 
{
	$komen.= "<li>".$value->comments ."</li>";
}
echo $komen."</ul>";
if(isset(Yii::app()->session['idUser']))
{
	echo "<textarea id='comment' cols='60' rows='5'></textarea><br /><br />";
	echo "<button id='tombol'>Comment</button>";
}

?>

<script type="text/javascript">
$(function() {
	
	console.log(comment);
	var gameID = '<?php echo $model->id; ?>';
	var userID = '<?php echo Yii::app()->session['idUser']; ?>';
	var url = '<?php echo "http://".$_SERVER["SERVER_NAME"]. Yii::app()->baseUrl."/games/GetFromAjax"; ?>';
	var comment = "";
	$("#tombol").click( function()
    {
    	
    	comment = $("#comment").val();
    	var formData = {comments:comment,game:gameID,user:userID};
    	$.ajax({
				    url : url,
				    type: "POST",
				    data : formData,
				    success: function(data, textStatus, jqXHR)
				    {
				        location.reload(true);
				    },
				    error: function (jqXHR, textStatus, errorThrown)
				    {
				 
				    }
				});
    });
});
</script>