<?php
  date_default_timezone_set('Asia/Jakarta');
 ?>
<?php $form = $this->beginWidget(
             'CActiveForm',
             array(
                 'id' => 'upload-form',
                 'enableAjaxValidation' => false,
                 'htmlOptions' => array('enctype' => 'multipart/form-data'),
             )
     ); ?>
<div class="form">


	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descr'); ?>
		<?php echo $form->textArea($model,'descr',array('cols'=>47,'rows'=>4)); ?>
		<?php echo $form->error($model,'descr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'developer'); ?>
		<?php echo $form->textField($model,'developer',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'developer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publisher'); ?>
		<?php echo $form->textField($model,'publisher',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'publisher'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'gameGenre'); ?>
		<?php echo $modelGenre; ?>
		<?php echo $form->error($model,'gameGenre'); ?>
	</div>	

	<div class="row">
		<?php echo $form->labelEx($model,'gameCover'); ?>
		<img src="<?php echo Yii::app()->baseUrl.'/images/'.$model->gameCover;  ?>" width=20px height=20px />
		<?php echo $form->fileField($model,'gameCover'); ?>
		<?php echo $form->error($model,'gameCover'); ?>
	</div>	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->