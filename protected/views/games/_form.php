<?php
  date_default_timezone_set('Asia/Jakarta');
 ?>
<?php $form = $this->beginWidget(
             'CActiveForm',
             array(
                 'id' => 'upload-form',
                 'enableAjaxValidation' => false,
                 'htmlOptions' => array('enctype' => 'multipart/form-data'),
             )
     ); ?>

<div class="form">
<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

  <div class="row">
    <?php echo $form->labelEx($model,'name'); ?>
    <?php echo $form->textField($model,'name',array(
                                                 'size'=>45,
                                                 'maxlength'=>45
    )); ?>
    <?php echo $form->error($model,'name'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'createDate'); ?>
    <?php echo $form->textField($model,'createDate',array(
                                                 'size'=>45,
                                                 'maxlength'=>45,
                                                 'readonly'=>'readonly',
                                                 'value' => date('Y-m-d:H:i:s'),
    )); ?>
    <?php echo $form->error($model,'createDate'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'creatorId'); ?>
    <?php echo $form->textField($model,'creatorId',array(
                                                 'size'=>45,
                                                 'maxlength'=>45,
                                                 'readonly'=>'readonly',
                                                 'value' => Yii::app()->session['idUser'],
    )); ?>                                                 
    <?php echo $form->error($model,'creatorId'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'descr'); ?>
    <?php echo $form->textField($model,'descr',array(
                                                 'size'=>45,
                                                 'maxlength'=>45
    )); ?>
    <?php echo $form->error($model,'descr'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'developer'); ?>
    <?php echo $form->textField($model,'developer',array(
                                                 'size'=>45,
                                                 'maxlength'=>45
    )); ?>
    <?php echo $form->error($model,'developer'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'publisher'); ?>
    <?php echo $form->textField($model,'publisher',array(
                                                 'size'=>45,
                                                 'maxlength'=>45
    )); ?>
    <?php echo $form->error($model,'publisher'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model,'genreID'); ?>
    <?php echo $modelGenre; ?>
    <?php echo $form->error($model,'genreID'); ?>
  </div>
 
  <div class="row">
    <?php echo $form->labelEx($model,'gameCover'); ?>
    <?php echo $form->fileField($model,'gameCover',array(
                                                   'size'=>45,
                                                   'maxlength'=>45
    )); ?>
    <?php echo $form->error($model,'gameCover'); ?>
  </div>
 
  <div class="row buttons">
    <?php echo CHtml::submitButton('Save'); ?>
    
  </div>
<?php $this->endWidget(); ?><!--CActiveForm widget-->