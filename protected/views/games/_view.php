<?php
/* @var $this GamesController */
/* @var $data Games */
// var_dump($data->creator->name);
?>

<div class="view">
	<?php echo CHtml::link("<img src='".Yii::app()->baseUrl.'/images/' .CHtml::encode($data->gameCover)."' width='110px' height='110px' />",array('view','id'=>$data->id)); ?>
	<br />
	<?php echo CHtml::encode($data->creator->name); ?>
	<br />
	<?php if(Yii::app()->session['idUser'] == $data->creatorId)
	{
	?>	
		<div class="row buttons">
			<?php echo CHtml::button('Edit', array('submit' => array('Games/update/'.$data->id))); ?>
		</div>	
	<?php } ?>
	

</div>