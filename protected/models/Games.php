<?php

/**
 * This is the model class for table "game".
 *
 * The followings are the available columns in table 'game':
 * @property integer $id
 * @property string $name
 * @property string $createDate
 * @property integer $creatorId
 * @property string $gameCover
 * @property string $descr
 * @property string $developer
 * @property string $publisher
 * @property integer $genreID
 *
 * The followings are the available model relations:
 * @property Users $creator
 * @property GameGenre $genre
 * @property GameComments[] $gameComments
 */
class Games extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Games the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, createDate, creatorId, gameCover, descr, developer, publisher, genreID', 'required'),
			array('creatorId, genreID', 'numerical', 'integerOnly'=>true),
			array('name, developer, publisher', 'length', 'max'=>50),
			array('gameCover', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, createDate, creatorId, gameCover, descr, developer, publisher, genreID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'creator' => array(self::BELONGS_TO, 'Users', 'creatorId'),
			'genre' => array(self::BELONGS_TO, 'GameGenre', 'genreID'),
			'gameComments' => array(self::HAS_MANY, 'GameComments', 'gameID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'createDate' => 'Create Date',
			'creatorId' => 'Creator',
			'gameCover' => 'Game Cover',
			'descr' => 'Descr',
			'developer' => 'Developer',
			'publisher' => 'Publisher',
			'genreID' => 'Genre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('createDate',$this->createDate,true);
		$criteria->compare('creatorId',$this->creatorId);
		$criteria->compare('gameCover',$this->gameCover,true);
		$criteria->compare('descr',$this->descr,true);
		$criteria->compare('developer',$this->developer,true);
		$criteria->compare('publisher',$this->publisher,true);
		$criteria->compare('genreID',$this->genreID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}